# Paragraphs usage

## Table of contents

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers

## Introduction

Verify if an a paragraph is used in specific content types.

## Requirements

This project requires the following:

- [Paragraphs](https://www.drupal.org/project/paragraphs)

## Installation

- Install as you would normally install a contributed Drupal module. For further
  information, see _[Installing Drupal Modules]_.

[Installing Drupal Modules]: https://www.drupal.org/docs/extending-drupal/installing-drupal-modules

## Configuration

- Enable the Paragraphs Usage on your site.
- Go to /admin/structure/paragraphs_type/{paragraphs_type}
- Click on Usage tab


## Maintainers

- [Ines WALLON (liber_t)](https://www.drupal.org/u/liber_t)
